This is the main part of the project proposal.

\subsection{Design Considerations}
  Because the impetus for this project was a shift in design thinking, there are
  several common features of music streaming services that will be re-examined.

  \begin{figure}
    \centering
    \subfigure{
      \includegraphics[width=1.5in]{imgs/problem}
      \label{fig:problem}
    }
    \subfigure{
      \includegraphics[width=1.5in]{imgs/prototype}
    }
    \caption{One common problem, and a possible solution}
    \label{fig:grouping}
  \end{figure}

  \subsubsection{Work/Performance Paradigm}
    The library for this project will be organized by composer and work. A given
    work will have multiple performances, where a performance maps to a specific
    collection of tracks. A single-movement work will have performances of one
    track. These performances may be identified by artist, album, and/or date.

  \subsubsection{Community Tagging}
    To make music more manageable, tags are commonly used to label certain
    aspects of each piece. Pieces need to be able to have tags specific to the
    user, but with suggestions based on the popularity of a given tag. If
    a large percentage of users give a piece a specific tag, a user should be
    able to either accept or reject that tag for his personal use. Most tags
    will probably be acceptable without much consideration, so the process
    should be fast and as invisible as possible. However, some tags like
    ``Sacred'' or "Secular" may be more controversial and require some user input.

  \subsubsection{Smart Transitions}
    Some music streaming services attempt to improve their user experience
    around the transitions between one track and the next by either fading
    between the two or removing empty space at the end of a track. These
    features are appropriate for most genres of pop music; however classical
    music typically serves a different purpose to the listener than pop
    music\cite{Cone68}, and these sorts of transitions impede that purpose. This
    service will do everything it can to make sure that the purpose and intent
    behind different types of music are fulfilled. Custom transitions between
    pieces will not be applied to classical music.

  \subsubsection{Smart Display}
    If multiple movements of a single work are being displayed together, then
    the information about the over-arching work should only be displayed once
    in a manner that clearly indicates its relationship to the movements it
    includes (see~Fig.~\ref{fig:grouping}). The name of a track should vary
    depending on the context it is display in. If it is being displayed in the
    web app where there will be plenty of room, then that room should be
    utilized to make all the information clear. However, if the track name is
    being displayed on a smaller display, for example a display on an external
    speaker system, then all necessary information should be included in a
    minified form.

\subsection{Known Unknowns}
  There are some critical features of this project that will require some
  research, since they fall outside the experience of this developer. The first
  investigation required involves the technical nuances of streaming music,
  instead of a direct download. Given the large number of audio and video
  streaming services available, we have high confidence that this problem is
  solveable, but pinning numbers on it is difficult.

\subsection{Proposed Toolsets}
  % Change "I recommend" to "This project will"
  This project should be built so that it can easily scale if it needs to
  support heavy traffic. The server-side logic will be written with
  Phoenix\cite{Phoenix}, a web framework for Elixir\cite{Elixir}. The database
  will need to support many queries over complicated data such as tags.
  Postgres\cite{Postgres} will be used for its special collection types.

  This proposal comes during an exciting time for web development. Native
  support for new standards is improving drastically, and there are many new
  native tools available to web developers. This project will implement its
  web app with new technologies such as native Web Components\cite{WebComp},
  and ESnext, with minimal outside dependencies.

  Source code will be managed and tracked with git, with team collaboration
  occuring on GitHub\cite{GitHub}. It will be impractical for the whole project
  to be opensourced, however Web Components make it easy to open source pieces
  of the project. As such, the main project repository will be private, but have
  dependencies on other public repositories maintained by the team.

  % Additional toolsets will be used for testing as described in section 4.
  The project should be tested throughout development using both automated
  and manual methods. This will require testing libraries for both Elixir and
  Javascript. For more details, see Section~\ref{sec:testing}.
  
\subsection{Milestones}
  % Make the numbers consistent
  % Make the team roles more clear
  This timeline is designed for a team of three working for a total of 300 man
  hours. Developers may wind up working on features from either the current or
  next milestones, depending on available work. Milestones beyond the next
  should be closed for development, and efforts should be made to close the
  current milestone before moving on to the next.

  \subsubsection{Project Setup}
    In the first milestone, the team will work on setting up the development and
    testing environments that we will be using throughout the project. This will
    include individual development envionments on each develment machine and a
    shared testing environment. All development machines will need to support
    the full project, and all team members should be comfortable with the
    environment used.

  \subsubsection{Library Browsing}
    This milestone will define the structure of the library, with performance
    mapped to pieces mapped to composers. This will include some basic
    interfaces for browsing this library, both as a platform for later
    development and as a tool for testing the library structure.

  \subsubsection{Library Search}
    Since browsing the database of all information is frequently slow and
    impractical, search is a critical feature. This milestone will utilize the
    models and structure developed in the previous milestone to provide an
    intelligent search experience.

  \subsubsection{Music Playback}
    This milestone will involve development of the streaming and controls for
    music playback. Controls for the queue will be stubbed out.

  \subsubsection{Playback queue}
    This milestone will include all features related to the play queue. At the
    end of this milestone listeners should be able to use common features like
    jumping to the next or previous pieces, looping, shuffling, etc.

  \subsubsection{Music Organization}
    At the end of this milestone users will be able to build their own
    playlists.

  \subsubsection{First Release}
    % Make this a sentence
    Final touches on minimum feature set, bug fixes, documentation, etc.

  \begin{table}
    \label{tbl:milestones}
    \caption{Projected Milestone Requirements}
    \centering
    \begin{tabular}{lc}
      Milestone & Hours \\
      \hline
      Project Setup & 10 \\
      Library Browsing & 40 \\
      Library Search & 40 \\
      Music Playback & 40 \\
      Playback Queue & 30 \\
      Music Organization & 40 \\
      First Release & 30 \\
      \hline
      Total & 230 \\
    \end{tabular}
  \end{table}
  
\subsection{Final Deliverables}
  The final deliverables for this project will include two server-side
  applications, and one client-side web app. One server-side application will
  be responsible for handling the media queries and actually streaming the
  music. Depending on server load, this application may be deployed in parallel
  with itself multiple times. The other server-side application will be
  responsible for all other requests that the web app may make, including CRUD
  instructions for the library database.

  The web app will be the default public face of this project. In future, I
  would also like to develop native clients, but a web app was chosen for this
  proposal for its ability to cover many devices and platforms with
  comparatively little effort.

\subsection{Needed Resources}
  % Make this a table
  This project will require money for several resources needed for development.
  The required budget is minimal, and all funds will be donated by the team
  members.

  \begin{itemize}
    \item The project as proposed will require a GitHub subscription for the
      hosting of a private repository, and an organizion to manage multiple
      public repositories.
    \item Hosted testing server. A digital ocean droplet will suffice.
  \end{itemize}

  \begin{table}
    \label{tbl:budget}
    \caption{Budget}
    \centering
    \begin{tabular}{lcr}
      Item & Provided By & Price \\
      Github Subscription & Team Members & \$25 \\
      Digital Ocean Droplet & Team Members & \$5 \\
      \hline
      Total (per month) & & \$30 \\
    \end{tabular}
  \end{table}
